#import "QMToolbarNavigationBar.h"

#define kNearZero 0.000001f

static void *QMToolbarNavigationBarContext = &QMToolbarNavigationBarContext;
static void *QMToolbarDrawerContext = &QMToolbarDrawerContext;

@interface QMToolbarNavigationBar ()
@property (assign, nonatomic) CGFloat lastContentOffsetY;
@end

@implementation QMToolbarNavigationBar

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		_drawer = [[BFNavigationBarDrawer alloc] init];
		_drawer.navigationBar = self;
	}
	return self;
}

- (void)setScrollView:(UIScrollView *)scrollView {
	_scrollView = scrollView;
	[_scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:QMToolbarNavigationBarContext];
	CGRect defaultFrame = self.frame;
	defaultFrame.origin.y = [self statusBarHeight];
	[self setFrame:defaultFrame alpha:1.0f animated:NO];
	_drawer.scrollView = _scrollView;
	[_drawer showFromNavigationBar:self animated:YES];
}

- (void)resetToDefaultPosition:(BOOL)animated {
	CGRect frame = self.frame;
	frame.origin.y = [self statusBarHeight];
	[self setFrame:frame alpha:1.0f animated:animated];
}

#pragma mark - notifications
- (void)statusBarOrientationDidChange {
	[self resetToDefaultPosition:NO];
}

- (void)applicationDidBecomeActive {
	[self resetToDefaultPosition:NO];
}

- (void)dealloc {
	[self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
}

- (CGFloat)statusBarHeight {
	switch ([UIApplication sharedApplication].statusBarOrientation) {
		case UIInterfaceOrientationPortrait:
		case UIInterfaceOrientationPortraitUpsideDown:
			return CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
			break;
            
		case UIInterfaceOrientationLandscapeLeft:
		case UIInterfaceOrientationLandscapeRight:
			return CGRectGetWidth([UIApplication sharedApplication].statusBarFrame);
            
		default:
			break;
	}
	return 0.0f;
}

- (void)scrollViewDidMoveWithDelta:(CGFloat)delta {
	if (self.scrollView.frame.size.height + (self.bounds.size.height * 2) >= self.scrollView.contentSize.height) {
		return;
	}
    
	CGFloat contentOffsetY = self.scrollView.contentOffset.y;
    
	if (contentOffsetY < -self.scrollView.contentInset.top) {
		return;
	}
    
	NSLog(@"contentOffset is %f", contentOffsetY);
    
	CGFloat deltaY = contentOffsetY - self.lastContentOffsetY;
	self.lastContentOffsetY = contentOffsetY;
    
	if (deltaY == 0) {
		self.scrollState = GTScrollNavigationBarNone;
		return;
	}
    
	if (deltaY < 0.0f) {
		self.scrollState = GTScrollNavigationBarScrollingDown;
	}
	else if (deltaY > 0.0f) {
		self.scrollState = GTScrollNavigationBarScrollingUp;
	}
    
    
    
	CGRect frame = self.frame;
	CGFloat alpha = 1.0f;
	CGFloat statusBarHeight = [self statusBarHeight];
	CGFloat maxY = statusBarHeight;
	CGFloat minY = maxY - CGRectGetHeight(frame) + 1.0f;
    
    
	UIPanGestureRecognizer *gesture = self.scrollView.panGestureRecognizer;
	if (gesture.state == UIGestureRecognizerStatePossible) {
		CGFloat contentOffsetYDelta = 0.0f;
		if (self.scrollState == GTScrollNavigationBarScrollingDown) {
			contentOffsetYDelta = maxY - frame.origin.y;
			frame.origin.y = maxY;
			alpha = 1.0f;
		}
		else if (self.scrollState == GTScrollNavigationBarScrollingUp) {
			contentOffsetYDelta = minY - frame.origin.y;
			frame.origin.y = minY;
			alpha = kNearZero;
		}
        
		[self setFrame:frame alpha:alpha animated:YES];
        
		if (!self.scrollView.decelerating) {
			CGPoint newContentOffset = CGPointMake(self.scrollView.contentOffset.x,
			                                       contentOffsetY - contentOffsetYDelta);
			[self.scrollView setContentOffset:newContentOffset animated:YES];
		}
		return;
	}
    
	if (self.scrollState == GTScrollNavigationBarScrollingUp && self.drawer.lastOffset >= -44.0) {
		return;
	}
    
	if (self.scrollState == GTScrollNavigationBarScrollingDown && self.drawer.lastOffset < 0) {
		return;
	}
    
	// NOTE: plus 1px to prevent the navigation bar disappears in iOS < 7
    
	frame.origin.y -= deltaY;
	frame.origin.y = MIN(maxY, MAX(frame.origin.y, minY));
    
	alpha = (frame.origin.y - (minY + statusBarHeight)) / (maxY - (minY + statusBarHeight));
	alpha = MAX(kNearZero, alpha);
    
	[self setFrame:frame alpha:alpha animated:NO];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"contentOffset"]) {
		CGPoint newPoint = [change[NSKeyValueChangeNewKey] CGPointValue];
		CGPoint oldPoint = [change[NSKeyValueChangeOldKey] CGPointValue];
		CGFloat delta = newPoint.y - oldPoint.y;
		[self scrollViewDidMoveWithDelta:delta];
	}
}

- (void)setFrame:(CGRect)frame alpha:(CGFloat)alpha animated:(BOOL)animated {
	if (animated) {
		[UIView beginAnimations:@"GTScrollNavigationBarAnimation" context:nil];
	}
    
	CGFloat offsetY = CGRectGetMinY(frame) - CGRectGetMinY(self.frame);
    
	for (UIView *view in self.subviews) {
		bool isBackgroundView = view == [self.subviews objectAtIndex:0];
		bool isViewHidden = view.hidden || view.alpha == 0.0f;
		if (isBackgroundView || isViewHidden)
			continue;
		view.alpha = alpha;
	}
	self.frame = frame;
    
	CGRect parentViewFrame = self.scrollView.superview.frame;
	parentViewFrame.origin.y += offsetY;
	parentViewFrame.size.height -= offsetY;
	self.scrollView.superview.frame = parentViewFrame;
    
	if (animated) {
		[UIView commitAnimations];
	}
}

@end

#define kAnimationDuration 0.3

typedef NS_ENUM (NSInteger, BFNavigationBarDrawerState) {
	BFNavigationBarDrawerStateHidden,   // The drawer is currently hidden behind the navigation bar, or not added to a view hierarchy yet.
	BFNavigationBarDrawerStateShowing,  // The drawer is sliding onto screen, from below the navigation bar.
	BFNavigationBarDrawerStateShown,    // The drawer is visible below the navigation bar and is not currently animating.
	BFNavigationBarDrawerStateHiding,   // The drawer is sliding off screen, back under the navigation drawer.
};

typedef NS_ENUM (NSInteger, BFScrollState) {
	BFScrollStateScrollingNone,
	BFScrollStateScrollingUp,
	BFScrollStateScrollingDown,
};

@implementation BFNavigationBarDrawer {
	UINavigationBar *_parentBar;
	NSLayoutConstraint *_verticalDrawerConstraint;
	BFNavigationBarDrawerState _state;
	CGFloat _lastContentOffsetY;
	BFScrollState _scrollState;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		[self setup];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super initWithCoder:decoder]) {
		[self setup];
	}
	return self;
}

- (id)init {
	self = [self initWithFrame:CGRectMake(0, 0, 320, 44)];
	return self;
}

- (void)setup {
	UIView *lineView = [[UIView alloc] init];
	lineView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
	lineView.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:lineView];
    
	NSDictionary *views = @{ @"line" : lineView };
	NSDictionary *metrics = @{ @"width" : @(1.0 / [UIScreen mainScreen].scale) }; // 1 physical pixel border high on any screen.
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:metrics views:views]];
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[line(width)]|" options:0 metrics:metrics views:views]];
    
	self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setScrollView:(UIScrollView *)scrollView {
	if (_scrollView != scrollView) {
		_scrollView = scrollView;
		UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
		panGestureRecognizer.delegate = self;
		[_scrollView addGestureRecognizer:panGestureRecognizer];
	}
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)gesture {
	if (gesture.state == UIGestureRecognizerStateEnded) {
		if (_scrollState == BFScrollStateScrollingUp) {
			[self showFromNavigationBar:self.navigationBar animated:YES];
			_lastOffset = 0.0f;
		}
		else if (_scrollState == BFScrollStateScrollingDown) {
			[self hideAnimated:YES];
			_lastOffset = -44.0f;
		}
	}
	CGFloat velocity = [gesture velocityInView:self.superview].y;
	NSLog(@"velocity: %f", velocity);
	if (velocity > 0.0f) {
		_scrollState = BFScrollStateScrollingUp;
	}
	else if (velocity < 0.0f) {
		_scrollState = BFScrollStateScrollingDown;
	}
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return YES;
}

- (void)setupConstraintsWithNavigationBar:(UINavigationBar *)bar {
	NSLayoutConstraint *constraint;
	constraint = [NSLayoutConstraint constraintWithItem:self
	                                          attribute:NSLayoutAttributeLeft
	                                          relatedBy:NSLayoutRelationEqual
	                                             toItem:bar
	                                          attribute:NSLayoutAttributeLeft
	                                         multiplier:1
	                                           constant:0];
	[self.superview addConstraint:constraint];
    
	constraint = [NSLayoutConstraint constraintWithItem:self
	                                          attribute:NSLayoutAttributeRight
	                                          relatedBy:NSLayoutRelationEqual
	                                             toItem:bar
	                                          attribute:NSLayoutAttributeRight
	                                         multiplier:1
	                                           constant:0];
	[self.superview addConstraint:constraint];
    
	constraint = [NSLayoutConstraint constraintWithItem:self
	                                          attribute:NSLayoutAttributeHeight
	                                          relatedBy:NSLayoutRelationEqual
	                                             toItem:nil
	                                          attribute:NSLayoutAttributeNotAnAttribute
	                                         multiplier:1
	                                           constant:44];
	[self addConstraint:constraint];
}

- (void)constrainBehindNavigationBar:(UINavigationBar *)bar {
	[self.superview removeConstraint:_verticalDrawerConstraint];
	_verticalDrawerConstraint = [NSLayoutConstraint constraintWithItem:self
	                                                         attribute:NSLayoutAttributeBottom
	                                                         relatedBy:NSLayoutRelationEqual
	                                                            toItem:bar
	                                                         attribute:NSLayoutAttributeBottom
	                                                        multiplier:1
	                                                          constant:0];
	[self.superview addConstraint:_verticalDrawerConstraint];
}

- (void)constrainBelowNavigationBar:(UINavigationBar *)bar {
	[self.superview removeConstraint:_verticalDrawerConstraint];
	_verticalDrawerConstraint = [NSLayoutConstraint constraintWithItem:self
	                                                         attribute:NSLayoutAttributeTop
	                                                         relatedBy:NSLayoutRelationEqual
	                                                            toItem:bar
	                                                         attribute:NSLayoutAttributeBottom
	                                                        multiplier:1
	                                                          constant:0];
	[self.superview addConstraint:_verticalDrawerConstraint];
}

- (void)constrainBelowNavigationBar:(UINavigationBar *)bar height:(CGFloat)height {
	[self.superview removeConstraint:_verticalDrawerConstraint];
	_verticalDrawerConstraint = [NSLayoutConstraint constraintWithItem:self
	                                                         attribute:NSLayoutAttributeTop
	                                                         relatedBy:NSLayoutRelationEqual
	                                                            toItem:bar
	                                                         attribute:NSLayoutAttributeBottom
	                                                        multiplier:1
	                                                          constant:height];
	[self.superview addConstraint:_verticalDrawerConstraint];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"contentOffset"]) {
		CGFloat contentOffsetY = self.scrollView.contentOffset.y;
		CGFloat deltaY = contentOffsetY - _lastContentOffsetY;
        
		if (_lastOffset <= 0 && _lastOffset >= -44.0 && [self.scrollView.panGestureRecognizer velocityInView:self].y != 0.0) {
			[self constrainBelowNavigationBar:self.navigationBar height:_lastOffset];
			_lastOffset = _lastOffset - deltaY;
		}
		else if (_lastOffset > 0) {
			_lastOffset = 0;
		}
		else if (_lastOffset < -44.0) {
			_lastOffset = -44.0;
		}
        
		NSLog(@"%f | %f", _lastOffset, deltaY);
        
		_lastContentOffsetY = contentOffsetY;
	}
}

- (void)showFromNavigationBar:(UINavigationBar *)bar animated:(BOOL)animated {
	if (bar == nil) {
		NSLog(@"Cannot display navigation bar from nil.");
		return;
	}
    
	if (_state == BFNavigationBarDrawerStateShown || _state == BFNavigationBarDrawerStateShowing) {
		return;
	}
    
	@try {
		[_scrollView removeObserver:self forKeyPath:@"contentOffset"];
	}
	@catch (NSException *exception)
	{
	}
    
	_parentBar = bar;
    
	[bar.superview insertSubview:self belowSubview:bar];
	[self setupConstraintsWithNavigationBar:bar];
    
	// Place the drawer behind the navigation bar at the beginning of the animation.
	if (animated && _state == BFNavigationBarDrawerStateHidden) {
		[self constrainBehindNavigationBar:bar];
	}
	[self.superview layoutIfNeeded];
    
	// This is a bit messy. Because navigation and toolbars are now translucent, we can't just resize the afftected scroll view
	// to make place for the drawer. Instead we have to change the contentInset property of the scroll view. Increasing the
	// contentInset.top value will make sure, that the drawer doesn't cover the first cell in a table view, but the scroll view
	// can still scroll under the drawer. Changing the contentInset however can't be animated unfortunately (or it can, by putting
	// it in an animation block, but that might break). To fix this, we have to animate the contentOffset property instead. First,
	// the contentOffset is changed in the opposite direction with no animation, basically negating the contentInset change. Then,
	// it is changed a second time, this time animated. Calculating the correct offsets is a bit tricky, because it depends on
	// whether or not the content size is bigger than the scroll view's height and whether the content is scrolled all the way to
	// the top, or the bottom. This calculation could probably be simplified, but this seems to be correct for now.
	CGFloat height = self.frame.size.height;
	CGFloat visible = _scrollView.bounds.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
	CGFloat diff = visible - _scrollView.contentSize.height;
	CGFloat fix = MAX(0.0, MIN(height, diff));
    
	// Increase the top inset of the affected scroll view, so the first cell is not covered by the drawer.
	UIEdgeInsets insets = _scrollView.contentInset;
	insets.top += height;
	_scrollView.contentInset = insets;
	_scrollView.scrollIndicatorInsets = insets;
	_scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, _scrollView.contentOffset.y + fix);
    
	[self constrainBelowNavigationBar:bar];
	void (^animations)() = ^void () {
		_state = BFNavigationBarDrawerStateShowing;
		[self.superview layoutIfNeeded];
		_scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, _scrollView.contentOffset.y - height);
	};
    
	void (^completion)(BOOL) = ^void (BOOL finished) {
		if (_state == BFNavigationBarDrawerStateShowing) {
			// Only change the state to shown, if the current state is showing. It could also be hiding, if hideAnimated: was
			// called before the showing animatation completed. In that case we would incorrectly set the state to shown.
			_state = BFNavigationBarDrawerStateShown;
		}
	};
    
	if (animated) {
		[UIView animateWithDuration:kAnimationDuration delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:animations completion:completion];
	}
	else {
		animations();
		completion(YES);
	}
	[_scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:QMToolbarNavigationBarContext];
}

- (void)setNavigationBar:(QMToolbarNavigationBar *)bar {
	_navigationBar = bar;
}

- (void)hideAnimated:(BOOL)animated {
	if (_state == BFNavigationBarDrawerStateHiding || _state == BFNavigationBarDrawerStateHidden) {
		NSLog(@"BFNavigationBarDrawer: Inconsistency warning. Drawer is already hiding or is hidden.");
		return;
	}
    
	if (!_parentBar) {
		NSLog(@"BFNavigationBarDrawer: Navigation bar should not be released while drawer is visible.");
		return;
	}
    
	@try {
		[_scrollView removeObserver:self forKeyPath:@"contentOffset"];
	}
	@catch (NSException *exception)
	{
	}
    
	// See that big comment block above.
	CGFloat height = self.frame.size.height;
	CGFloat visible = _scrollView.bounds.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
	CGFloat fix = height;
	if (visible <= _scrollView.contentSize.height - height) {
		CGFloat bottom = -_scrollView.contentOffset.y + _scrollView.contentSize.height;
		CGFloat diff = bottom - _scrollView.bounds.size.height + _scrollView.contentInset.bottom;
		fix = MAX(0.0, MIN(height, diff));
	}
	CGFloat offset = height - (_scrollView.contentOffset.y + _scrollView.contentInset.top);
	CGFloat topFix = MAX(0.0, MIN(height, offset));
    
	// Reverse the change to the inset of the affected scroll view.
	UIEdgeInsets insets = _scrollView.contentInset;
	insets.top -= height;
	_scrollView.contentInset = insets;
	_scrollView.scrollIndicatorInsets = insets;
	_scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, _scrollView.contentOffset.y - topFix);
    
	[self constrainBehindNavigationBar:_parentBar];
    
	void (^animations)() = ^void () {
		_state = BFNavigationBarDrawerStateHiding;
		[self.superview layoutIfNeeded];
		_scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, _scrollView.contentOffset.y + fix);
	};
    
	void (^completion)(BOOL) = ^void (BOOL finished) {
		if (_state == BFNavigationBarDrawerStateHiding) {
			// Only remove the drawer and update the state, if the current state is hiding. It could also be showing, if the showFromNavigationBar:animated:
			// method was called again, before the hiding animation completed. In that case we don't want to remove the drawer of course.
			_parentBar = nil;
			[self removeFromSuperview];
			_state = BFNavigationBarDrawerStateHidden;
		}
	};
    
	if (animated) {
		[UIView animateWithDuration:kAnimationDuration delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:animations completion:completion];
	}
	else {
		animations();
		completion(YES);
	}
	[_scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:QMToolbarNavigationBarContext];
}

@end
