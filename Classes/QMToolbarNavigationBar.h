@import UIKit;

@class QMToolbarNavigationBar;

typedef enum {
	GTScrollNavigationBarNone,
	GTScrollNavigationBarScrollingDown,
	GTScrollNavigationBarScrollingUp
} GTScrollNavigationBarState;

@interface BFNavigationBarDrawer : UIToolbar <UIGestureRecognizerDelegate>

@property (nonatomic, readonly, getter = isVisible) BOOL visible;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) QMToolbarNavigationBar *navigationBar;
@property CGFloat lastOffset;

- (void)showFromNavigationBar:(UINavigationBar *)bar animated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

@end

@interface QMToolbarNavigationBar : UINavigationBar

@property (nonatomic) BFNavigationBarDrawer *drawer;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (assign, nonatomic) GTScrollNavigationBarState scrollState;

@end
