Pod::Spec.new do |s|
  s.name             = "QMToolbarNavigationBar"
  s.version          = '0.1.0'
  s.summary          = "A navigation bar with a toolbar under it."
  s.description      = <<-DESC
                       And a messy thing you shouldn't probably look at.
                       DESC
  s.homepage         = "http://qmoya.com"
  s.license          = 'MIT'
  s.author           = { "Quico Moya" => "me@qmoya.com" }
  s.source           = { :git => "https://github.com/qmoya/QMToolbarNavigationBar.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/qmoya'

  s.platform     = :ios, '7.0'
  s.ios.deployment_target = '7.0'
  s.requires_arc = true

  s.source_files = 'Classes'
end
