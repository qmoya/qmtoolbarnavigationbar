# QMToolbarNavigationBar

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QMToolbarNavigationBar is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "QMToolbarNavigationBar"

## Author

Quico Moya, me@qmoya.com

## License

QMToolbarNavigationBar is available under the MIT license. See the LICENSE file for more info.

